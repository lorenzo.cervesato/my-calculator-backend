const express = require('express');
const app = express();
const port = 3001;

var cors = require('cors'); // Utilisé pour pouvoir faire des appels depuis un autre host.

app.use(express.json());

app.use(cors());

app.get('/*', (req, res) => {
  res.send("Bienvenu sur le backoffice, par contre c'est pas la bonne route...");
})

app.post('/calculate', (req, res) => {
 var toCalculate = req.body.toCalculate;
 var response = eval(toCalculate);
 res.send({ "response" : String(response) });
})

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
})
